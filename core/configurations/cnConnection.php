<?php
use Medoo\Medoo;

$database = new Medoo([
    'database_type' => DB_TYPE,
    'database_name' => DB_NAME,
    'server' => DB_SERVER,
    'username' => DB_USER,
    'password' => DB_PASS,
    'charset' => DB_CHARSET
]);