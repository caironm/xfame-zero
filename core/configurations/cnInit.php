<?php

header ('Content-type: text/html; charset=uft-8');
/* --------------------------------------------------------------------------
| After developing: ini_set('display_errors', 0);
-------------------------------------------------------------------------- */
ini_set('display_errors', 1);
error_reporting(~0);

/* --------------------------------------------------------------------------
| Define Time-Zone Referencial
-------------------------------------------------------------------------- */
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

/* --------------------------------------------------------------------------
| Twig configurations
-------------------------------------------------------------------------- */
$loader = new Twig_Loader_Filesystem('app/views'); 
$twigConfig = array(
    // 'cache'	=>	'./cache/twig/',
    // 'cache'	=>	false,
	'debug'	=>	true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function($twig) {
	$twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

/* --------------------------------------------------------------------------
| Basic Definitions
-------------------------------------------------------------------------- */
define("BASE_URL",              "/learn/xframe-zero/");
define("SITE",                  "http://localhost:8888/learn/xframe-zero/");
define("PATH_TO_C",             "app/controllers/");
define("PATH_TO_M",             "app/models/");
define("PATH_TO_V",             "app/views/");
define("CHARSET",               "UTF-8");

/* --------------------------------------------------------------------------
| Define database
-------------------------------------------------------------------------- */
define("DB_TYPE",               "mysql");
define("DB_NAME",               "base");
define("DB_SERVER",             "localhost");
define("DB_USER",               "user");
define("DB_PASS",               "pass");
define("DB_CHARSET",            "uft8");

/* --------------------------------------------------------------------------
| Email configurations
-------------------------------------------------------------------------- */
/* 

define("MAIL_HOST",             "smtp.server.com");
define("MAIL_SMTPAUTH",         true);
define("MAIL_USERNAME",         "email@server.com");
define("MAIL_PASSWORD",         "password");
define("MAIL_SMTPSECURE",       "ssl");
define("MAIL_PORT",             465);

define("MAIL_NAME_SENDER",     "Name of Sender");
define("MAIL_NOREPLY",         "no-reply@server.com"); 

*/

/* --------------------------------------------------------------------------
| Path for functions and helpers
-------------------------------------------------------------------------- */
define("CONNECTION",            "core/configurations/cnConnection.php");

define("MIDDLEWARE",            "core/functions/fnMiddleware.php");
define("CHARACTERS",            "core/functions/fnCharacters.php");
define("VALIDATE",              "core/functions/fnValidate.php");
define("SEND_EMAIL",            "core/functions/fnSend_Email.php");

/* --------------------------------------------------------------------------
| Template for all e-mails
-------------------------------------------------------------------------- */
/*

define("MAIL_LOGIN_ACTIVE",    "files/emails/login/mail_Active.php");
define("MAIL_LOGIN_RECOVERY",  "files/emails/login/mail_Recovery.php");

*/