<?php
require_once("vendor/phpmailer/phpmailer/class.phpmailer.php");

$mail = new PHPMailer();

$mail->IsSMTP();
$mail->Host = MAIL_HOST;
$mail->SMTPAuth = MAIL_SMTPAUTH;
$mail->Username = MAIL_USERNAME; 
$mail->Password = MAIL_PASSWORD;
$mail->SMTPSecure = MAIL_SMTPSECURE;
$mail->Port = MAIL_PORT;

$mail->From = $email_from;
$mail->FromName = $email_from_name;
$mail->AddAddress($email_to);
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Coppy
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Hidden coppy

$mail->IsHTML(true);
$mail->CharSet = CHARSET; // Charset of messenge (opcional)

$mail->Subject  = $email_subject;
$mail->Body = $email_body;
$mail->AltBody = $email_altbody;
//$mail->AddAttachment("c:/temp/document.pdf", "new_name.pdf");

$email_send = $mail->Send();
$mail->ClearAllRecipients();
$mail->ClearAttachments();