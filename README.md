[Copie e cole todo o conteúdo deste documento em dillinger.io para visualizar](http://dillinger.io/)

# xFRAME - Guia de início rápido

### O QUE É
Não é um framework e sim, um set de configurações módular para se comportar como um, sem deixar a documentação e a organização de lado. Esse produto não reinventa a roda mas concerteza, vai fazer você sentir que desenvolveu sua própria roda. O maior benefífio é a liberdade para criar soluções e fazer adaptações em uma aplicação veloz e segura.

### A QUEM SE DESTINA / OBJETIVO
Startups e grande coorporações. Promete rápidez para criar soluções com alto grau de complexidade e expansão.

###### Para o projeto
Instale apache, php e sql ao seu modo (xampp, wamp ou individual), instale o composer, o git, o node js, npm, bower.

### INSTALAÇÃO
Crie uma pasta com o nome do seu projeto e baixe este conteúdo dentro, agora siga estes passos:
1. Rode o terminal, ou cmd para windows na pasta que colocou os arquivos do projeto.
2. Digite: `composer install`
3. Digite: `bower install`
4. Agora, para que o envio de e-mail funcione localmente *__habilide a extensão open_openssl e php_sockets__** no php.ini, basta abrir o php.ini e usando `Ctrl+F` busque pelos termos openssl, descomente e depois depois repita os procedimentos para sockets, não se esqueça de reiniciar o apache depois do procedimento;
5. Crie uma tabela com usuário e senha e importe a tabela do projeto.

### AJUSTES
Todos os ajustes que precisarão ser feitos estão em core/configurations/cnInit.php

### REGRAS
Existem 2 principais regras para que tudo funcione bem.
* Respeite as regras das bibliotecas existentes e das que você adicionar;
* Respeite o fluxo de organização do projeto. (Essa segunda regra parece bem trapaceira pois exige conhecimento sobre o fluxo, mas não será um problema);

Um bom exemplo da aplicação de regras. A biblioteca de orm padrão para esse projeto funciona muito bem em qualquer parte da aplicação desde que não seja colocada dentro de classes ou funções. As bibliotecas já possui susas próprias funções e classes.

### ESTRUTURA
Você vai se deparar com 4 pastas principais, depois que tudo tiver instalado: **_app_**, **_core_**, **_public_** e **_vendor_**. Se você já usou algum framework MVC fica mais fácil para entender. 
* Em **_app_** : **_controllers_**, **_models_** e **_views_**;
* Na pasta **_core_** : **_configuractions_**, **_functions_** e **_languages_**;
* Na pasta **_public_** : os arquivos comumente ao front-end;
* **_vendor_** com as biliotecas do php.

SEGUE PARTE DA ESTRUTURA
- seu-projeto
    - **_app_**
         - controllers
         - models
         - views
    - **_core_**
        - configurations
        - functions
        - files
    - **_public_**
         - css
         - fontes
         - icons
         - images
         - js
         - libs
         - uploader
    - **_vendor_**
    - **_.bowerrc_**
    - **_.editorconfig_**
    - **_.gitignore_**
    - **_.htaccess_**
    - **_bower.json_**
    - **_composer.json_**
    - **_database.mwb_**
    - **_database.sql_**
    - **_index.php_**
    - **_README.md_**

Explicação: **_.bowerrc_** é responsável em definir a pasta que instalaremos nossas dependência para **_bower.json_**. O arquivo **_.editorconfig_** é um arquivo de configuração para a ide de desenvolvimento que dita o espaçamento de tabs. Se você trabalha com git, não vai querer enviar a pasta das bibliotecas que você gerencia, então o **_.gitignore_**, lista os diretórios a se evitar. O **_.htaccess_** é especial, porque foi feito para ser lido em quase todas as hospedagens. Todas as bibliocas php são gerenciadas aqui: **_composer.json_**. Os arquivos **_database_** foram incluidos por causa do exemplo incluso, o tipo **_.mwb_** é para o software **_MySQL Workbech_**. **_index.php_** roda nossa aplicação e **_README.md_** é este arquivo que esplica tudo.

### CONVENÇÕES
Convenções adotadas no ambiente de trabalho para o projeto XFrame.
* Regra para nome de classes e tabelas e colunas em um db: **_primeiroNome_**
* Para as tabelas devem possuir 's' no final;
* Todas as `classes` devem ser representadas por um substantivos e não um verbo: Clientes, Contatos, Perfil, Configurações etc;
* Todos os `metodos` devem ser representadas por um verbo ou frases verbais no infinitivo: salvarStatus, editarPagina, remover etc;
* Regra para nome de links: **_editar/pagina/sou-um-link_**;
* Dentro das principais pastas do projetos há dois arquivos, **_.htaccess_** e **__index.html__**, ele bloqueiam o acesso para a pasta e os subdiretórios;
* mIsso_E_Um_Model.php, vIsso_E_Um_View.twig, c_Isso_E_Um_Controller.php. Models começam com m e assim sucessivamente;
* Não é possível agrupar a pasta de views em subpastas, com excessão de subpastas para templates, por esse fato a view recebe essa nomenclatura **__v + Nome do modulo + Pagina do módulo__**, como por ex.: vTodo_List, vTodo_Edit.
* Arquivos de configuração começam com **cn**, funções **fn** e traduções **lg** de languages. Exemplo: cnConnection.php, fnValidate.php, lgBR.php
* Deu pra perceber que está tudo em inglês, é uma boa idéia manter assim para deixar a applicação universal desde o começo.

EXEMPLO DE APLICAÇÃO
- controllers
    - api
        - cHome.php
    - login
        - cActive.php
        - cLogin.php
        - cLogout.php
        - cRecovery.php
        - cRegister.php
    - site
        - cHome.php
- models
    - login
        - mCheck_User.php
        - mRegister_New_User.php
        - mCheck_Key.php
        - mGet_Id_User_By_Key.php
        - ...
- views
    - vApi_Home.twig
    - vSite_Home.twig
    - vLogin_Active.twig
    - vLogin_Login.twig
    - vLogin_New_Pass.twig
    - vLogin_Recovery.twig
    - vLogin_Register.twig

### BIBLIOTECAS DO PROJETO
* Usamos o [ Flight framework ](http://flightphp.com/) como framework para rotas, porque tem um desempenho melhor que o Slim Framework ou outros top de linha;

* Banco de dados MySql e biblioteca [ Medoo ](http://medoo.in/). O Medoo é de longe a melhor library que ja conheci. A regra de outro é que suas clases não podem de maneira alguma ficarem dentro de classes e ou funções;

* Outras bibliotecas: o [ Twig ](http://twig.sensiolabs.org) para gerenciar e separar com mais segurança as views e o clássico PHP Mailer para envio de e-mails, Docktrine Cache para aplicações com o a cúmulo de dados em cache, pagseguro etc.

### ROTAS
Pense em uma coisa simples para manusear. Veja documentação oficial das rotas em: [ http://flightphp.com ](http://flightphp.com/learn/)
```
Flight::route('/login', function() {

	session_start();
	if (isset($_SESSION['passport']) OR isset($_COOKIE['passport'])) {
		Flight::redirect('/api'); #### REDIRECT HERE
	}
	else{

		require CONNECTION;
		require VALIDATE;
		require LANGUAGE;

		$data = array(
			'base_url' => BASE_URL,
			'alert' => 'Utilize o arquivo lang para passar as variáveis que não vierem de um db',
			'link_recovery' => BASE_URL.'recovery'
		);
		$query = array_merge($data, $lang);
		Flight::view()->display('vLogin_Login.twig', $query);

	}
});
/* ====================================================================
======================================================================= */
Flight::route('POST /register/gear', function() {
...
});
/* ====================================================================
======================================================================= */
Flight::route('/register/feedback/@feedback', function($feedback){

	require LANGUAGE;

	switch ($feedback) {
		case "email":
			$fb = $lang['lang_fb_email'];
			break;
		case "wrongpass":
			$fb = $lang['lang_fb_wrongpass'];
			break;
		case "registred":
			$fb = $lang['lang_fb_registred'];
			break;
		default:
			$fb = "";
	}

	$data = array(
		'base_url' => BASE_URL,
		'feedback' => $fb
	);

	$query = array_merge($data, $lang);
	Flight::view()->display('vLogin_Register.twig', $query);
});
```

```
<!DOCTYPE html>
<html>
<body>

<h1>Logar</h1>

<form action="{{ base_url }}login/gear" method="post">
  Email:<br>
  <input type="text" name="email">
  <br>
  Senha:<br>
  <input type="password" name="pass">
  <br><br>
  <input type="checkbox" name="remember" value="yes" checked="checked" >Lembrar de mim<br>
  <br>
  <input type="submit" value="Enviar">
  <br><br>
  {{ feedback }}
</form>
<a href="{{ link_recovery }}">Esqueci a senha</a>

</body>
</html>
```
### BANCO DE DADOS
Medoo é sem dúvidas a ORM mais simples que eu já pude conhecher na vida.
Você pode estudar a aplicação de login desenvolvida e consultar o guia oficial em: [ medoo.in ](http://medoo.in).

### SISTEMA DE TEMPLATE
Escolhi twig pela facilidade de aprendizado, por ter usado bastante com microframeworks também. Se você olhar uma [ folha de resumo ](https://s-media-cache-ak0.pinimg.com/originals/9b/7c/f0/9b7cf0ed69f91af8bdbf3d55ec5f893e.jpg) sobre o twig dá pra tirar muitas dúvidas, contudo, se você já está acostumado com com o Smarty por exemplo pode substituir.

----------------------------
#### SE VOCÊ CHEGOU ATÉ AQUI
Muito obrigado pela atenção

#### SOBRE O AUTOR/ORGANIZADOR
Philipe Cairon Medeiros de Siqueira
contato@caironm.com
Idéias? Vamos discutir. Whats.: 84 93300.0966
